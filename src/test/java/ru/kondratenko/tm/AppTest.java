package ru.kondratenko.tm;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;

import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() {
        final Application application = new Application();
        final Long userId = application.getUserService().findByLogin("admin").getId();
        final Long diffUserId = application.getUserService().findByLogin("test").getId();
        System.out.println("START TEST");
        System.out.println(application.getProjectController().listProject());
        System.out.println(application.getTaskController().listTask());
        System.out.println();
        System.out.println("FIND TASKS BY NAME 1task1");
        System.out.println(application.getTaskService().findByName("1task1",userId));
        System.out.println("FIND PROJECT BY TASK NAME 1task1 - EMPTY");
        System.out.println(application.getProjectService().findByName("1task1",userId));
        System.out.println("FIND PROJECT BY NAME Cproject1");
        System.out.println(application.getProjectService().findByName("Cproject1",userId));
        System.out.println("FIND PROJECT BY NAME Cproject1 WITH DIFFERENT USER ");
        System.out.println(application.getProjectService().findByName("Cproject1",diffUserId));
        System.out.println();

        System.out.println("REMOVE TASK BY NAME 1task1");
        application.getTaskService().removeByName("1task1",userId);
        application.getTaskController().listTask();
        System.out.println("REMOVE TASK BY PROJECT NAME Cproject1 - list is not changed");
        application.getTaskService().removeByName("Cproject1",userId);
        application.getTaskController().listTask();
        System.out.println("REMOVE PROJECT BY NAME Cproject1");
        application.getProjectService().removeByName("Cproject1",userId);
        application.getProjectController().listProject();
        System.out.println(application.getProjectService().findByName("Cproject1",userId));
        System.out.println("REMOVE TASK BY NAME Otask2 WITH DIFFERENT USER");
        application.getTaskService().removeByName("Otask2",diffUserId);
        application.getTaskController().listTask();
        System.out.println();

        System.out.println("UPDATE PROJECT INDEX 1 AND FIND BY NAME");
        Project project = application.getProjectService().findByIndex(1,userId);
        application.getProjectService().update(project.getId(),"newname","",userId);
        System.out.println(application.getProjectService().findByName("newname",userId));

        System.out.println("UPDATE PROJECT INDEX 1 WITH DIFFERENT USER AND LIST - NOT CHANGE");
        application.getProjectService().update(project.getId(),"newnameDU","",diffUserId);
        application.getProjectController().listProject();

        System.out.println("UPDATE TASK INDEX 1 AND LIST");
        Task task = application.getTaskService().findByIndex(1,userId);
        application.getTaskService().update(task.getId(),"newTaskname","",userId);
        application.getTaskController().listTask();
        System.out.println(application.getTaskService().findByName("newTaskname",userId));


//        application.run("version");
//        application.run("version");
//        application.run("version");
//        application.run("about");
//        application.run("version");
//        application.run("version");
//        application.run("version");
//        application.run("version");
//        application.run("version");
//        application.run("version");
//        System.out.println();
//        application.run("history");

        assertTrue(true);
    }

}
