package ru.kondratenko.tm.controller;

import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.service.ProjectService;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.service.ProjectTaskService;
import ru.kondratenko.tm.service.UserService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;

    private final ProjectTaskService projectTaskService;

    private final UserService userService;

    public ProjectController(ProjectService projectService, ProjectTaskService projectTaskService, UserService userService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
        this.userService = userService;
    }

    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT NAME:]");
        final String name = scanner.nextLine();
        System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
        final String description = scanner.nextLine();
        if (userService.currentUser == null)projectService.create(name, description);
        else projectService.create(name, description, userService.currentUser.getId());
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT INDEX:]");
        final Long userId = userService.currentUser.getId();
        final Project project = projectService.findByIndex(inputIndexCheckFormat(),userId);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER PROJECT NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
            final String description = scanner.nextLine();
            projectService.update(project.getId(), name, description,userId);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("[PLEASE, ENTER PROJECT ID:]");
        final Long userId = userService.currentUser.getId();
        final Project project = projectService.findById(inputIdCheckFormat(),userId);
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            System.out.println("[PLEASE, ENTER PROJECT NAME:]");
            final String name = scanner.nextLine();
            System.out.println("[PLEASE, ENTER PROJECT DESCRIPTION:]");
            final String description = scanner.nextLine();
            projectService.update(project.getId(), name, description,userId);
            System.out.println("[OK]");
        }
        return 0;
    }

    public int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        if(userService.currentUser == null){
        projectService.clear();
        projectTaskService.clear();}
        else{
            for(Project project: projectService.findAllByUserId(userService.currentUser.getId())){
                projectService.removeById(project.getId(),userService.currentUser.getId());}
        }
        System.out.println("[OK]");
        return 0;
    }

    public int removeProjectByIndex() {
        System.out.println("[CLEAR PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX: ");
        final Project project = projectService.removeByIndex(inputIndexCheckFormat(),userService.currentUser.getId());
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
            for (final Task task : tasks) {
                projectTaskService.removeTaskFromProject(project.getId(), task.getId());
            }
            System.out.println("[OK]");
        }
        return 0;
    }

    public int removeProjectByName() {
        System.out.println("[CLEAR PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME: ");
        final String name = scanner.nextLine();
        final List<Project> projects = projectService.removeByName(name,userService.currentUser.getId());
        if (projects == null) {
            System.out.println("[FAIL]");
        } else {
            for(Project project : projects){
                final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
                for (final Task task : tasks) {
                    projectTaskService.removeTaskFromProject(project.getId(), task.getId());
                }
            }
            System.out.println("[OK]");
        }
        return 0;
    }

    public int removeProjectById() {
        System.out.println("[CLEAR PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID: ");
        final Project project = projectService.removeById(inputIdCheckFormat(),userService.currentUser.getId());
        if (project == null) {
            System.out.println("[FAIL]");
        } else {
            final List<Task> tasks = projectTaskService.findAllByProjectId(project.getId());
            for (final Task task : tasks) {
                projectTaskService.removeTaskFromProject(project.getId(), task.getId());
            }
            System.out.println("[OK]");
        }
        return 0;
    }

    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

    public int viewProjectByIndex() {
        System.out.println("ENTER PROJECT INDEX: ");
        final Project project = projectService.findByIndex(inputIndexCheckFormat(),userService.currentUser.getId());
        viewProject(project);
        return 0;
    }

    public int viewProjectByName() {
        System.out.println("ENTER PROJECT NAME: ");
        final List<Project> projects = projectService.findByName(scanner.nextLine(),userService.currentUser.getId());
        viewProjects(projects);
        return 0;
    }

    public int viewProjectById() {
        System.out.println("ENTER PROJECT ID: ");
        final Project project = projectService.findById(inputIdCheckFormat(),userService.currentUser.getId());
        viewProject(project);
        return 0;
    }

    public int listProject() {
        System.out.println("[LIST PROJECT]");
        int index = 1;
        List<Project> projectList;
        if (userService.currentUser == null) {
            projectList = projectService.findAll();}
        else {
            projectList = projectService.findAllByUserId(userService.currentUser.getId());
        }
        viewProjects(projectList);
        return 0;
    }

    public void viewProjects(final List<Project> projects) {
        if (projects == null || projects.isEmpty()) {
            System.out.println("[PROJECTS ARE NOT FOUND]");
            return;
        }
        int index = 1;
        Collections.sort(projects, new Comparator<Project>() {
            public int compare(Project o1, Project o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        for (final Project project : projects) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");
    }

}
