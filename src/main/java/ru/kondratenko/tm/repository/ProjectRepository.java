package ru.kondratenko.tm.repository;

import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectRepository {

    private List<Project> projects = new ArrayList<>();

    private HashMap<String,List<Project>> projectsA = new HashMap<>();

    public Project addToMap(final Project project){
        List<Project> projectsInMap = projectsA.get(project.getName());
        if (projectsInMap == null) projectsInMap = new  ArrayList<>();
        projectsInMap.add(project);
        projectsA.put(project.getName(),projectsInMap);
        return project;
    }

    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        addToMap(project);
        return project;
    }

    public Project create(final String name, final String description) {
        final Project project = new Project(name, description);
        projects.add(project);
        addToMap(project);
        return project;
    }

    public Project create(final String name, final String description, final Long userId) {
        final Project project = new Project(name, description, userId);
        projects.add(project);
        addToMap(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description, final Long userId) {
        Project project = findById(id,userId);
        if (project == null) return null;
        String oldName = project.getName();
        List<Project> projectListOld = findByName(oldName,userId);
        if(projectListOld == null) return null;
        project.setName(name);
        project.setDescription(description);
        if(!oldName.equals(name) && projectListOld.size()>1) {
            List<Project> projectListNew = new ArrayList<>();
            projectListNew.add(project);
            projectListOld.remove(project);
            projectsA.remove(oldName);
            projectsA.put(oldName,projectListOld);
            projectsA.put(name,projectListNew);
        }else{
            projectsA.remove(oldName);
            projectsA.put(name,projectListOld);
        }
        return project;
    }

    public Project findByIndex(final int index, final Long userId) {
        List<Project> result;
        if (userId == null) result = findAll();
        else result = findAllByUserId(userId);
        if (index < 0 || index > result.size() - 1) return null;
        return result.get(index);
    }

    public List<Project> findByName(final String name, final Long userId) {
        if (userId == null) return projectsA.get(name);
        List<Project> result = new ArrayList<>();
        if(projectsA.get(name) == null) return null;
        for (Project project: projectsA.get(name)){
            if(project.getUserId().equals(userId)) result.add(project);
        }
        return result;
    }

    public Project findById(final Long id,final Long userId) {
        List<Project> currentListTask;
        if (userId == null) currentListTask = findAll();
        else currentListTask = findAllByUserId(userId);
        for (final Project project : currentListTask) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public Project removeByIndex(final int index, final Long userId) {
        Project project = findByIndex(index,userId);
        if (project == null) return null;
        List<Project> projectFromName =findByName(project.getName(),userId);
        projectsA.remove(project.getName());
        projectFromName.remove(project);
        projectsA.put(project.getName(),projectFromName);
        return project;
    }

    public Project removeById(final Long id, final Long userId) {
        Project project = findById(id,userId);
        if (project == null) return null;
        List<Project> projectFromName =findByName(project.getName(),userId);
        projectsA.remove(project.getName());
        projectFromName.remove(project);
        projectsA.put(project.getName(),projectFromName);
        return project;
    }

    public List<Project> removeByName(final String name,  final Long userId) {
        List<Project> projectList = findByName(name,userId);
        if (projectList == null || projectList.size() == 0) return null;
        projects.removeAll(projectList);
        projectsA.remove(name);
        return projectList;
    }

    public void clear() {
        projects.clear();
        projectsA.clear();
    }

    public List<Project> findAll() {
        return projects;
    }

    public List<Project> findAllByUserId(final Long userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project : findAll()) {
            final Long IdUser = project.getUserId();
            if (IdUser == null) continue;
            if (IdUser.equals(userId)) result.add(project);
        }
        return result;
    }

}
