package ru.kondratenko.tm.repository;

import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;

import java.util.*;

public class TaskRepository {

    private List<Task> tasks = new ArrayList<>();

    private HashMap<String,List<Task>> tasksA = new HashMap<>();

    public Task addToMap(final Task task){
        List<Task> tasksInMap = tasksA.get(task.getName());
        if (tasksInMap == null) tasksInMap = new  ArrayList<>();
        tasksInMap.add(task);
        tasksA.put(task.getName(),tasksInMap);
        return task;
    }

    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        addToMap(task);
        return task;
    }

    public Task create(final String name, final String description) {
        final Task task = new Task(name, description);
        tasks.add(task);
        addToMap(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId) {
        final Task task = new Task(name, description, userId);
        tasks.add(task);
        addToMap(task);
        return task;
    }

    public Task update(final Long id, final String name, final String description, final Long userId ) {
        Task task = findById(id,userId);
        if (task == null) return null;
        String oldName = task.getName();

        List<Task> taskListOld = findByName(oldName,userId);
        if(taskListOld == null) return null;
        task.setName(name);
        task.setDescription(description);
        if(!oldName.equals(name) && taskListOld.size()>1) {
            List<Task> taskListNew = new ArrayList<>();
            taskListNew.add(task);
            taskListOld.remove(task);
            tasksA.remove(oldName);
            tasksA.put(oldName,taskListOld);
            tasksA.put(name,taskListNew);
        }else{
            tasksA.remove(oldName);
            tasksA.put(name,taskListOld);
        }
        return task;
    }
    

    public Task findByIndex(int index,final Long userId) {
        List<Task> result;
        if (userId == null) result = findAll();
        else result = findAllByUserId(userId);
        if (index < 0 || index > result.size() - 1) return null;
        return result.get(index);
    }

    public List<Task> findByName(final String name, final Long userId) {
        if (userId == null) return tasksA.get(name);
        List<Task> result = new ArrayList<>();
        if(tasksA.get(name) == null) return null;
        for (Task task: tasksA.get(name)){
            if(task.getUserId().equals(userId)) result.add(task);
        }
        return result;
    }

    public Task findById(final Long id,final Long userId) {
        List<Task> currentListTask;
        if (userId == null) currentListTask = findAll();
        else currentListTask = findAllByUserId(userId);
        for (final Task task : currentListTask) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        for (final Task task : tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task removeByIndex(final int index, final Long userId) {
        Task task = findByIndex(index,userId);
        if (task == null) return null;
        tasks.remove(task);
        List<Task> taskFromName =findByName(task.getName(),userId);
        tasksA.remove(task.getName());
        taskFromName.remove(task);
        tasksA.put(task.getName(),taskFromName);
        return task;
    }

    public Task removeById(final Long id, final Long userId) {
        Task task = findById(id,userId);
        if (task == null) return null;
        tasks.remove(task);
        List<Task> taskFromName =findByName(task.getName(),userId);
        tasksA.remove(task.getName());
        taskFromName.remove(task);
        tasksA.put(task.getName(),taskFromName);
        return task;
    }

    public List<Task> removeByName(final String name,final Long userId) {
        List<Task> taskList = findByName(name,userId);
        if (taskList == null || taskList.size() == 0) return null;
        tasks.removeAll(taskList);
        tasksA.remove(name);
        return taskList;
    }

    public void clear() {
        tasks.clear();
        tasksA.clear();
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public List<Task> findAllByUserId(final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long IdUser = task.getUserId();
            if (IdUser == null) continue;
            if (IdUser.equals(userId)) result.add(task);
        }
        return result;
    }

    public List<Task> findAll() {
        return tasks;
    }

}
