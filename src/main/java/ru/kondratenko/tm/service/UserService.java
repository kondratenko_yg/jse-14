package ru.kondratenko.tm.service;

import ru.kondratenko.tm.entity.User;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.repository.UserRepository;

import static ru.kondratenko.tm.util.HashUtil.*;

import java.util.List;

public class UserService {
    private final UserRepository userRepository;

    public User currentUser;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User create(
            String login, String password,
            String firstName, String lastName, Role role)
    {
        if (login == null) return null;
        if (password == null) return null;
        if (firstName == null) return null;
        if (lastName == null) return null;
        if (role == null) return null;
        return userRepository.create(login, hashMD5(password), firstName, lastName, role);
    }

    public User create(Long id, String login, String password, Role role) {
        if (id == null) return null;
        if (login == null) return null;
        if (password == null) return null;
        if (role == null) return null;
        return userRepository.create(login, hashMD5(password), role);
    }

    public boolean checkPassword(final User user, final String password) {
        return user.getPassword().equals(hashMD5(password));
    }

    public User findByLogin(String login) {
        if (login == null) return null;
        return userRepository.findByLogin(login);
    }

    public User findById(Long id) {
        if (id == null) return null;
        return userRepository.findById(id);
    }

    public User updateByLogin(String login, String password, String firstName, String lastName) {
        if (login == null) return null;
        if (password == null) return null;
        if (firstName == null) return null;
        if (lastName == null) return null;
        return userRepository.updateByLogin(login, hashMD5(password), firstName, lastName);
    }

    public User updateById(Long id, String password, String firstName, String lastName) {
        if (id == null) return null;
        if (password == null) return null;
        if (firstName == null) return null;
        if (lastName == null) return null;
        return userRepository.updateById(id, hashMD5(password), firstName, lastName);
    }

    public User updatePasswordByLogin(String login, String password) {
        if (login == null) return null;
        if (password == null) return null;
        return userRepository.updatePasswordByLogin(login, hashMD5(password));
    }

    public User updatePasswordById(Long id, String password) {
        if (id == null) return null;
        if (password == null) return null;
        return userRepository.updatePasswordById(id, hashMD5(password));
    }

    public User removeByLogin(String login) {
        if (login == null) return null;
        return userRepository.removeByLogin(login);
    }

    public User removeById(Long id) {
        if (id == null) return null;
        return userRepository.removeById(id);
    }

    public void clear() {
        userRepository.clear();
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

}
